import React, { Component } from 'react';
import List from './List';
import Tasks from './data/Task';

class Todo extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            items: Tasks
        })
    }

    render() {
        const items = [...this.state.items];

        items.push({id: 'xyz-abc', name: 'test copy', level: 1})

        return (
            <div className="panel panel-success">
                <div className="panel-heading">
                    <span className="panel-title">
                        List Task
                    </span>
                </div>
                <div className="panel-body">
                    <List items={items} />
                </div>
            </div>
        )
    }
}

export default Todo;