import React, {Component} from "react";

class Item extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {item, index} = this.props

        return (
            <tr>
                <th scope="row">{index + 1}</th>
                <td>{item.name}</td>
                <td>
                    {item.level}
                </td>
                <td>
                    <div className="btn-group">
                        <button className="btn btn-warning">Edit</button>
                        <button className="btn btn-danger">Delete</button>
                    </div>
                </td>
            </tr>
        )

    }
}

export default Item;