const uuidv4 = require('uuid/v4');
let tasks = [
    {id: uuidv4(), name: 'Php', level: 0}, // 0->small, 1 ->medium, 2->high
    {id: uuidv4(), name: 'Javascript', level: 1},
    {id: uuidv4(), name: 'Nodejs', level: 2}
];

export default tasks;