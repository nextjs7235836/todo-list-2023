import React, { Component } from "react";
import Item from './Item';

class List extends Component {

    render(){

        const items = this.props.items;

        const elmItem = items.map((item, index) => {
            return (
                <Item item={item} index={index} key={index} />
            )
        })

        return (
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Task</th>
                        <th scope="col">Level</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {elmItem}
                </tbody>
            </table>
        )

    }
}

export default List;